'use strict';

const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const Joi = require('@hapi/joi');
const Pack = require('./package');

var mqtt = require('mqtt');

// Options de connexion au serveur MQTT
let options = {
    host: '35.240.44.110',
    port: 8883,
    protocol: "mqtt",
    prtocolId: 'MQTT',
    username: 'maxime',
    password: 'Maxime'
};

var client = mqtt.connect(options);

const init = async () => {

    // Configuration du serveur
    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    // Configuration de la documentation de l'API
    const swaggerOptions = {
        info: {
            title: 'Test API Documentation',
            version: Pack.version,
        },
    };

    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);

    // Accueil route
    server.route({
        method: 'GET',
        path: '/',
        handler: (request, h) => {
            return 'Hello World!';
        }
    });

    // Route pour la réception d'un gesture et l'envoie sur Server MQTT
    server.route({
        method: 'POST',
        path: '/gestures',
        options: {
            description: 'SendToMQTT()',
            notes: 'Send gesture interpretation to Angerona MQTT Server',
            tags: ['api'],
            handler: (request) => {
                if (request.payload) {
                    console.log(request.payload);
                    client.publish('gesture/' + request.payload.action.actionType, request.payload.action.actionLabel);
                    return ({"Gesture Action" : request.payload.action.actionType, "Instruction" : request.payload.action.actionLabel});
                } else {
                    return ({"Server Response": "Gesture not recognized"});
                }
            },
            validate: {
                payload: Joi.object({
                    name: Joi.string(),
                    img: Joi.string(),
                    possibility: Joi.number(),
                    action: Joi.object({
                        actionType: Joi.string(),
                        actionLabel: Joi.string()
                    })
                })
            }
        }
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

// Dés la connection au serveur MQTT on peut lancer le serveur
client.on("connect", function () {
    console.log("client connecté");
    console.log('Client mosquitto info: USER: ' + client.options.username + " | CLIENTID: " + client.options.clientId);
    init();
});

// Affichage de l'erreur de connexion au serveur Angerona-MQTT Mosquitto
client.on("error", function (error) {
    console.log("Can't connect to Angerona-MQTT: " + error);
});